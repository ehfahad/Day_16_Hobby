<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP1020\Hobby\Hobby;

$obj =new Hobby();
$obj->prepare($_GET);

$array = $obj->edit();

$singleItem= $obj->view();

//echo $singleItem->id;
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Hobbies</h2>

    <form role="form" method="post" action="update.php">
        <input type="hidden" name="id" value="<?php echo $singleItem->id ?>">
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Playing Cricket" <?php if(in_array("Playing Cricket",$array) ): ?> checked <?php endif ?>  >Playing Cricket</label>
        </div>
        <div class="checkbox disabled">
            <label><input type="checkbox" name=hobby[] value="Playing Football" <?php if(in_array("Playing Football",$array) ): ?> checked <?php endif ?>>Playing Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Gaming" <?php if(in_array("Gaming",$array) ): ?> checked <?php endif ?>>Gaming</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Watching Movies" <?php if(in_array("Watching Movies",$array) ): ?> checked <?php endif ?>>Watching Movies</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Hanging Out" <?php if(in_array("Hanging Out",$array) ): ?> checked <?php endif ?>>Hanging Out</label>
        </div>
        <input type="submit" value="Submit">
    </form>
</div>

</body>
</html>

