<?php


include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP1020\Book\Utility;
use App\Bitm\SEIP1020\Hobby\Hobby;

//Utility::d($_POST);

$selected= $_POST['hobby'];

$hobbies = implode(",", $selected);
//echo $hobbies;

$_POST['hobby'] = $hobbies;

$obj = new Hobby();
$obj->prepare($_POST);
$obj->update();
